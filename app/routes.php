<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


//http://localhost/crud_laravel/public/crud/show para ver todos los posts
Route::get('inicio', 'PostController@inicio');
//http://localhost/crud_laravel/public/crud/create para ver formulario crear post
Route::get('nuevo-post', 'PostController@create');
//http://localhost/crud_laravel/public/crud/update/id para ver el formulario actualizar
Route::get('detalle-post/{id}', 'PostController@read');




// //grupo de rutas que aceptan peticiones post, protegemos de ataques csrf
Route::group(array('before' => 'csrf'), function()
{
	Route::post('nuevo-post', 'PostController@create');
	Route::post('actualizar/{id}', 'PostController@update');
	Route::post('detalle-post/{id}', 'PostController@read');
	Route::post('login', 'AuthController@logIn');
	Route::post('usuario', 'AuthController@registrar');

});


Route::get('login', 'AuthController@logIn');
Route::get('usuario', 'AuthController@registrar');
Route::get('logOut', 'AuthController@logOut');

// Nos indica que las rutas que están dentro de él sólo serán mostradas si antes el usuario se ha autenticado.
Route::group(array('before' => 'auth'), function()
{
    // Esta será nuestra ruta de bienvenida.
    Route::get('/', function()
    {
        return View::make('hello');
    });
    // Esta ruta nos servirá para cerrar sesión.
    Route::get('logout', 'AuthController@logOut');
    Route::post('perfil', 'AuthController@perfil');

    Route::post('votar', 'VotoController@votar');
    Route::post('difamar', 'VotoController@difamar');

    Route::get('perfil', 'AuthController@perfil');
    Route::get('actualizar/{id}', 'PostController@update');
	//http://localhost/crud_laravel/public/crud/delete/id para eliminar post
	Route::get('borrar/{id}', 'PostController@delete');

});

