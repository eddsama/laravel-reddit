<?php
/**
* 
*/
class Comentario extends Eloquent
{
	
	protected $fillable = array("contenido", "FK_post","FK_user");
	protected $softDelete = true;

	public function post(){
		return $this->belongsTo('Post', 'FK_post');
	}

	public function user(){
		return $this->belongsTo('User', 'FK_user');
	}


}
