<?php
/**
* 
*/
class Post extends Eloquent
{
	
	protected $fillable = array("titulo","contenido","FK_user");
	protected $softDelete = true;

	public function user(){
		return $this->belongsTo('User', 'FK_user');
	}
	public function comentario(){
		return $this->hasMany('Comentario', 'FK_post');
	}
	public function voto(){
		return $this->hasMany('Voto', 'FK_post');
	}

}
