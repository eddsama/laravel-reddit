<?php
/**
* 
*/
class Voto extends Eloquent
{
	
	protected $fillable = array('FK_user', 'FK_post');

	public function user(){
		return $this->belongsTo('User', 'FK_user');
	}

	public function post(){
		return $this->belongsTo('Post', 'FK_post');
	}
}