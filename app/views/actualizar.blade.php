@extends('plantillas.base')

@section('titulo')
	Actualizar Publicacion
@stop

@section('cuerpo')
	<div class="row">
		<div class="col-sm-3">
			
		</div>
		<div class="col-sm-5 table-bordered" id="borde">
			{{ Form::open(array('url' => 'actualizar/'.$post->id, 'rol' => 'form')) }}
				<div class="form-group">
					{{ Form::label('nueva-publicacion', 'Nueva Publicación', array('class' => 'control-label')) }}
				</div>
				<div class="form-group">
					{{ Form::text('titulo', Input::old('titulo') ? Input::old('titulo') : $post->titulo,
									 array('class' => 'form-control', 'placeholder' => 'Titulo')) }}
				</div>
				<div class="form-group">
					{{ Form::textarea('contenido', Input::old('contenido') ? Input::old('contenido') : $post->contenido,
									 array('class' => 'form-control', 'placeholder' => 'todo Code Here')) }}
				</div>
				<div class="form-group">
					{{ Form::submit('Enviar Publicación', array('class' => 'form-control btn-primary')) }}
				</div>
			{{ Form::close() }}
		</div>
		<div class="col-sm-4">
			@if($errors->has())
                <div class="alert-box alert">           
                    <!--recorremos los errores en un loop y los mostramos-->
                    @foreach ($errors->all('<p>:message</p>') as $message)
                        {{ $message }}
                    @endforeach
                     
                </div>
            @endif
		</div>
@stop