@extends('plantillas.base')

@section('titulo')
	Nueva Publicación
@stop

@section('cuerpo')
	<div class="row">
		<div class="col-sm-3">
			
		</div>
		<div class="col-sm-5 table-bordered" id="borde">
			{{ Form::open(array('url' => 'nuevo-post', 'rol' => 'form')) }}
				<div class="form-group">
					{{ Form::label('nueva-publicacion', 'Nueva Publicación', array('class' => 'control-label')) }}
				</div>
				<div class="form-group">
					{{ Form::text('titulo', '', array('class' => 'form-control',
													  'placeholder' => 'Titulo',
													  'autofocus')) }}
				</div>
				<div class="form-group">
					{{ Form::textarea('contenido', '', array('class' => 'form-control', 'placeholder' => 'todo Code Here')) }}
				</div>
				<div class="form-group">
					{{ Form::submit('Enviar Publicación', array('class' => 'form-control btn-primary')) }}
				</div>
			{{ Form::close() }}
		</div>
		<div class="col-sm-4">
			@if($errors->has())
                    <!--recorremos los errores en un loop y los mostramos-->
                    @foreach ($errors->all('<div class="alert alert-danger"><p>:message</p></div>') as $message)
                        {{ $message }}
                    @endforeach
            @endif
		</div>
	</div>
@stop