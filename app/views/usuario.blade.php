@extends('plantillas.base')

@section('titulo')
	Registro de Usuario
@stop

@section('cuerpo')
	<div class="row">
		<div class="col-sm-4">
			@if($errors->has())
					@foreach($errors->all('<div class="alert alert-danger"><p>:message</p></div>') as $message)
						{{ $message }}
					@endforeach
			@endif
		</div>
		<div class="col-sm-4 table-bordered" id="borde">
			
				<h3>Crear una Cuenta</h3>
				{{ Form::open(array('url' => 'usuario', 'role' => 'form')) }}
					<div class="form-group">
						{{ Form::label('usuario', 'Nombre de Usuario') }}
						{{ Form::text('usuario', '', array('class' => 'form-control', 
															'placeholder' => 'nombre de Usuario',
															'autofocus')) }}
					</div>
					<div class="form-group">
						{{ Form::label('email', 'E-mail') }}
						{{ Form::email('email', '', array('class' => 'form-control', 'placeholder' => 'E-mail')) }}
					</div>
					<div class="form-group">
						{{ Form::label('password', 'Contraseña') }}
						{{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'contraseña')) }}
					</div>
					<div class="form-group">
						{{ Form::label('password2', 'Confirmar Contraseña') }}
						{{ Form::password('password2', array('class' => 'form-control', 'placeholder' => 'confirmar contraseña')) }}
					</div>
					<div class="form-group">
						{{ Form::submit('Crear Registro', array('class' => 'btn btn-primary')) }}
					</div>
				{{ Form::close() }}	
			
		</div>
		<div class="col-sm-4">
			@if(Session::has('mensaje_error'))
                <div class="alert alert-danger">{{ Session::get('mensaje_error') }}</div>
            @endif
		</div>
	</div>
@stop