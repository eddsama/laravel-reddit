@extends('plantillas.base')

@section('titulo')
	Home
@stop

@section('cuerpo')
	<div class="row">
		<div class="col-sm-8">
			@if(Session::has('mensaje'))
	                <div>
	                    {{ Session::get('mensaje') }}
	                </div>
	        @endif
			<div class="panel-group">
	            @if(count($posts) > 0)
	           
	                @foreach($posts as $post)
		                	<div class="col-sm-12 table-bordered">
		                		<div class="panel-default">
		                		<table >
		                			<tr>
		                			<td width="50">
		                			
		                				@if(Auth::User() && Auth::User()->id != $post->user->id)
			                				{{ Form::open(array('url' => 'votar')) }}	
				                				<button class="votar" type="submit" name="like" value="{{ $post->id }}" >
						                			<span class="glyphicon glyphicon-plus" id="like"></span>
						                		</button>
						                		{{ Form::hidden('post',$post->id) }}

						                	{{ Form::close() }}
						                @endif

						                {{ Form::text('numVotos',$post->like, array('id' => 'numVotos')) }}

						                @if(Auth::User() && Auth::User()->id != $post->user->id)
						                	{{ Form::open(array('url' => 'difamar')) }}
						                		<button class="votar" type="submit" name="dislike" value="{{ $post->id }}" >
						                			<span class="glyphicon glyphicon-minus" id="dislike"></span>
						                		</button>
						                		{{ Form::hidden('post',$post->id) }}

						                	{{ Form::close() }}
					                	@endif
				                	</td>
				                	<td>
				                		<div class="panel-heading">


				                			<img src="{{ $post->user->foto }}" id="foto-miniatura">
				                			<strong>{{ HTML::link(URL::to('detalle-post/'.$post->id), $post->titulo) }}</strong>
				                			<small>Publicado por: {{ $post->user->usuario }}.</small>		
				                			<small>el {{ $post->updated_at }}</small>	                				
				                		</div>
			                			<div class="panel-body">
			                				<p>{{ $post->contenido }}</p>
			                			</div>
			                		</td>
		                			</tr>
		                		</table>
		                		</div>
		                	</div>
	                @endforeach
	            @else
	            	<h2>No Existen registros</h2>
	            @endif  
	        </div> 
	            
		</div>
		<div class="col-sm-4 table-bordered" id="borde">
			
				<div class="form-group">
					@if(Auth::check())
						Puede {{ HTML::link(URL::to('logOut'), 'Salir') }}	para avandonar el sitio.
					
					@endif
				</div>
				<div class="form-group">
					<!-- {{ Form::button('Subir Nueva Publicación', array('class' => 'btn btn-primary', 'url' => 'nuevo-post')) }} -->
					@if(Auth::check())
						{{ HTML::link(URL::to('nuevo-post'), 'subir nueva publicación') }}
					@endif
					<img src="img/Like.jpeg">
				</div>
				<!-- 
				<div class="form-group">
					{{ HTML::link(URL::to('usuario'), 'ver la imagen') }}
				</div> 
				-->
		</div>

	</div>

	
@stop
