@extends('plantillas.base')

@section('titulo')
	Perfil
@stop

@section('cuerpo')
	<!-- {{ var_dump($foto) }} -->

	<div class="row">
		<div class="col-sm-5 table-bordered" id="borde">
			{{ Form::open(array('url' => 'perfil', 'files' => true)) }}
				<div class="col-sm-5">
					<div class="table-bordered" id="imgFrame">
					
 						<img src="{{ $foto }}" id="foto-perfil">
					
					</div>
					{{ Form::file('foto') }}
				</div> 
				<div class="col-sm-7">
					<div class="row">
						{{ Form::label('usuario','usuario') }}<br>
						{{ Form::text('usuario',Auth::User()->usuario, array('border' => 'none',
																			'size' => '30',
																			'id' => 'lblperfil')) }}  
						
					</div>
					<div class="row">
						{{ Form::label('email','E-mail') }}<br>
						{{ Form::text('email',Auth::User()->email, array('border' => 'none',
																			'size' => '30',
																			'id' => 'lblperfil')) }} 
						
					</div>
					<div class="row">
						
				        {{ Form::submit('Actualizar Datos') }}
					</div>
				</div>
			{{ Form::close() }}
		</div>
		<div class="col-sm-7">
			<div class="row">
				<div class="col-sm-12 table-bordered">
					{{ HTML::link(URL::to('nuevo-post'), 'subir nueva publicación') }}
				</div>
			</div>
			@if(count($posts) > 0)
				@foreach($posts as $post)
				<div class="row">
					<div class="col-sm-12 table-bordered">
		                <div class="panel-default">
			                <div class="panel-heading">
			                	<strong>{{ HTML::link(URL::to('detalle-post/'.$post->id), $post->titulo) }}</strong>
			                	<small>Publicado por: {{ $post->user->usuario }}</small>
			                	{{ HTML::link(URL::to('actualizar/'.$post->id), 'Actualizar post') }} 
		                       	{{ HTML::link(URL::to('borrar/'.$post->id), 'Eliminar post') }}			                				
			                </div>
			           	 	<div class="panel-body">
			                	<p>{{ $post->contenido }}</p>
			                </div>
		                </div>
		            </div>
		        </div>
				@endforeach
			@else
	            <h2>No Existen registros</h2>
	        @endif 
		</div>
	</div>
	<div class="row">
			@if(Session::has('mensaje'))
				<div class="alert alert-success">{{ Session::get('mensaje') }}</div>
			@elseif(Session::has('mensaje_error'))
				<div class="alert alert-success">{{ Session::get('mensaje_error') }}</div>
			@endif	
			
		</div>
@stop