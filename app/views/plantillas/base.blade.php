<!DOCTYPE HTML>
<html>
<head>
	<title>
		@section('titulo')
			laravel - reddit
		@show
	</title>
    
	{{ HTML::style('css/bootstrap.css') }}
    {{ HTML::style('css/bootstrap-theme.css') }}
    {{ HTML::style('css/estilo.css') }}

</head>
<body>
	<!-- Container -->
    <div class="container">
       	<div class="row" style="font: oblique bold 120% serif; background-color: #CEF6F5">
			<div class="col-sm-6">
                <h1>{{ HTML::link(URL('inicio'), 'reddit') }}</h1>
            </div>
            <div class="col-sm-3" >
            </div>
            <div class="col-sm-1" >
                @if(Auth::check())
                    <a href="../perfil">
                        <img src="{{ Auth::User()->foto }}" id="foto-front">
                    </a>
                @endif
            </div>
            <div class="col-sm-2" >
                @if(Auth::check())
                    <h5>Bienvenid@ {{ HTML::link(URL('perfil'), Auth::user()->usuario) }}</h5>
                    <h6>{{ HTML::link(URL('logOut'), 'Salir') }}</h6>
                @else
                    <h6>Puede crear una {{ HTML::link(URL('usuario'), 'Nueva cuenta') }} o {{ HTML::link(URL('login'), 'Registrarse') }} en pocos segundos.</h6>
                @endif
            </div>
		</div>
        <!-- Content -->
        @yield('cuerpo')

    </div>

    <!-- Scripts are placed here -->
    {{ HTML::script('js/jquery-1.11.1.min.js') }}
    {{ HTML::script('js/bootstrap.min.js') }}
    <!-- {{ HTML::script('js/validarVotos.js') }} -->
</body>
</html>