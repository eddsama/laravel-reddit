@extends('plantillas.base')

@section('titulo')
	Detalle de Publicación
@stop

@section('cuerpo')
	<div class="row">
		<div class="col-sm-1">
			
		</div>
		<div class="col-sm-11">
			<div class="form-group">
				<div class="col-sm-12 table-bordered">
		            <div class="panel-primary">
		            	<div class="panel-heading">
		            	<table>
			            	<tr>
			            		<td>
					            	<img src="{{ $post->user->foto }}" id="foto-post">
					            </td>
					            <td>
				            		<h2>{{ $post->titulo }}</h2>
				            		<small>Por: {{ $post->user->usuario }}</small><br>
				            		<small>Ultima Actualización: {{ $post->updated_at }}</small>
				            	</td>
			            	</tr>
			            </table>
		            	</div>
		            	<div class="panel-body">
		               		<p>{{ $post->contenido }}</p>
		               		
		               		@if(Auth::check() && Auth::user()->id == $post->FK_user)
		                		{{ HTML::link(URL::to('actualizar/'.$post->id), 'Actualizar post') }} 
		                       	{{ HTML::link(URL::to('borrar/'.$post->id), 'Eliminar post') }}
		                   	@endif
		               	</div>
		        	       	@if(count($comentarios) > 0)
		               			@foreach($comentarios as $comentario)
		               				<div class="panel-body ">

		               					<img src="{{ $comentario->user->foto }}" id="foto-miniatura">
		               					<strong>{{ $comentario->user->usuario }}</strong><br>		               					<small>{{ $comentario->updated_at }}</small>
		               					<p>{{ $comentario->contenido }}</p>
		               				</div>
		               			@endforeach
		               		@endif
		                <div class="panel-footer">
		                	@if(Auth::check())
		                		{{ Form::open(array('url' => 'detalle-post/'.$post->id, 'rol' => 'form')) }}
		                			<div class="form-group">
		                				{{ Form::textarea('comentario', '', 
		                								array('placeholder' => 'TODO Comentario aquí',
		                										'size' => '118x5')) }}
		                			</div>
		                			<div class="form-group">
		                				{{ Form::submit('Comentar') }}
		                				@if(Session::has('mensaje'))
		                					<div class="alert alert-danger">{{ Session::get('mensaje') }}</div>
		                				@endif
		                			</div>
		                		{{ Form::close() }}
		                	@endif
		               	</div>

		            </div>
		        </div>
			
			</div>
		</div>
	</div>
@stop