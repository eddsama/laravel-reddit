<?php
/**
* 
*/
class AuthController extends BaseController
{
	public function logIn(){
		//post
		if (Input::get()){
			$userdata = array(
            'usuario' => Input::get('usuario'),
            'password'=> Input::get('password')
	        );
	        // Validamos los datos y además mandamos como un segundo parámetro la opción de recordar el usuario.
	        if(Auth::attempt($userdata, Input::get('recordar', 0)))
	        {
	            // De ser datos válidos nos mandara a la bienvenida
	            return Redirect::to('inicio');
	        }
	        // En caso de que la autenticación haya fallado manda un mensaje al formulario de login y también regresamos los valores enviados con withInput().
	        return Redirect::to('login')
	                    ->with('mensaje_error', 'Tus datos son incorrectos')
	                    ->withInput();

		}else{//get
			if (Auth::check()){//ususario registrado
				return Redirect::to('inicio');
			}else{
				return View::make('login');
			}
		}
	}
	
	public function registrar(){

		if (Input::get()) {
			
			if ($this->validateForms(Input::all()) === true){
				if (Input::get('password') == Input::get('password2')) {
					if (User::where('email','=',Input::get('email'))->count() == 0) {
						$user = new User();
						$user->usuario = Input::get('usuario');
						$user->email = Input::get('email');
						$user->password = Hash::make(Input::get('password'));
						$user->foto = "img/default.jpeg";

						if ($user->save()){
						return Redirect::to('login')->with('mensaje', 'Cuenta creada exitosamente');
						}
					}else {
						return Redirect::to('usuario')->with('mensaje_error', 'Esta cuenta de correo ya esta en uso')->withInput();	
					}
				} else {
					return Redirect::to('usuario')->with('mensaje_error', 'Las contraseñas no coinciden')->withInput();
				}
			}else{
			 	return Redirect::to('usuario')->withErrors($this->validateForms(Input::all()))->withInput();
			}	
		} else {
			return View::make('usuario');
		}
	}

    public function logOut()
    {
        Auth::logout();
        Session::flush();
        return Redirect::to('inicio')
                    ->with('mensaje_error', 'Tu sesión ha sido cerrada.');
    }

    public function perfil(){

    	//$posts = Auth::User()->post;
    	$posts = Post::where('FK_user', '=', Auth::User()->id)->orderBy('updated_at', 'DESC')->get();
    	$user = Auth::User();
    	if (Input::get()) {

    		$file = Input::file('foto');
    		$user->usuario = Input::get('usuario');
    		$user->email = Input::get('email');
    		$user->foto = '../img/'.$file->getClientOriginalName();
    		if ($user->save()) {
    			
    			$file->move("public/img",$file->getClientOriginalName());
    			return Redirect::to('perfil')->with(array('foto' => $user->foto,
    														'mensaje' => 'Datos Guardados'));
    		} else {
    			return Redirect::to('perfil')->with(array('mensaje_error' => 'No se pudo guardar la imagen'));
    		}	
    	} else {
    		return View::make('perfil')->with(array('posts' => $posts, 'foto' => $user->foto));
    	}
    	
    }

    private function validateForms($inputs = array()){
	    
	    $rules = array(
	        'usuario'  	=> 'required|min:3|max:100',
	        'email'   => 'required|min:10|max:100|email'
	    );
	        
	    $messages = array(
	        'required'  => 'El campo :attribute es obligatorio.',
	        'min'       => 'El campo :attribute no puede tener menos de :min carácteres.',
	        'max'       => 'El campo :attribute no puede tener más de :min carácteres.',
	        'email'		=> 'El campo :attribute no es valido'
	    );
    
    	$validation = Validator::make($inputs, $rules, $messages);
 
    	if($validation->fails()){
    		return $validation;
    	}else{
    		return true;
    	}
 
	}
}