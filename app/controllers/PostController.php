<?php
/**
* 
*/
class PostController extends BaseController
{
	
	public function inicio(){
		//cargar los post Crear modelo Post
		//$user = User::where('id','=',$post->FK_user)
		//$posts = Post::all()->orderBy('updated_at', 'DESC')->get();
		$votos = Voto::all();//where('FK_post','=','11')->get();
		$posts = Post::where('id', '>', '0')->orderBy('like', 'DESC')->get();
		return View::make('inicio')->with('posts',$posts)
								   ->with('votos',$votos);
	}

	public function create(){

		//Peticion post con datos
		if (Input::get()) {
			
			if($this->validateForms(Input::all()) === true){

				$post = new Post();
				$post->titulo = Input::get('titulo');
				$post->contenido = Input::get('contenido');
				$post->FK_user = Auth::user()->id;

				if ($post->save()) {
					return Redirect::to('inicio')->with(array('mensaje' => 'NUEVO!!'));
				}
			}else{
				return Redirect::to('nuevo-post')->withErrors($this->validateForms(Input::all()))->withInput();
			}
		}else{
			return View::make('nuevo-post');
		}
	}

	public function read($id){

		$post = Post::find($id);
		$comentarios = $post->comentario;
		if (Input::get()) {

			if ($post) {
				//return Redirect::to($this->revisarComentario(array('comentario' => Input::get('comentario')), $id, Auth::User()->id));
				if (Input::get('comentario') != null) {
					$comentario = new Comentario();
					$comentario->contenido = Input::get('comentario');
					$comentario->FK_post = $id;
					$comentario->FK_user = Auth::User()->id;

					if ($comentario->save()) {
						$comentarios = $post->comentario;
						return Redirect::to('detalle-post/'.$id)->with('comentarios')
																->with('comentarios', $comentarios);
					} else {
						return Redirect::to('detalle-post/'.$id)->with(array('mensaje' => 'no se pudo almacenar'))
																->with('comentarios', $comentarios);
					}
					
				} else {
					return Redirect::to('detalle-post/'.$id)->with(array('mensaje' => 'comentario vacio'))
															->with('comentarios', $comentarios);
				}
				
			}else{
				return Redirect::to('inicio')->with(array('mensaje' => 'El post no existe'));
			}
		}else {
			return View::make('detalle-post', array('post' => $post, 'comentarios' => $comentarios));
		}
	}
	public function update($id){

		$post = Post::find($id);
		if (Input::get()) {
			if ($post) {
				if ($this->validateForms(Input::all()) === true) {
					
					$post->titulo = Input::get('titulo');
					$post->contenido = Input::get('contenido');

					if($post->save()){
						return Redirect::to('inicio')->with(array('mensaje' => 'Actualizado'));
					}
				}else{
					return Redirect::to('actualizar/'.$id)->withErrors($this->validateForms(Input::all()))->withInput();
				}
			}else{
				return Redirect::to('inicio')->with(array('mensaje' => 'El post no existe'));
			}
		}else{
			return View::make('actualizar', array('post' => $post));
		}

	}

	public function delete($id){

		$post = Post::find($id);
		if ($post) {
			$post->delete();
			return Redirect::to('inicio')->with(array('mensaje' => 'eliminado completo'));
		}else{
			return Redirect::to('inicio')->with(array('mensaje' => 'no existe el post a eliminar'));
		}
	}
	
	private function validateForms($inputs = array()){
	    
	    $rules = array(
	        'titulo'  	=> 'required|min:2|max:100',
	        'contenido'   => 'required|min:5|max:255',
	    );
	        
	    $messages = array(
	        'required'  => 'El campo :attribute es obligatorio.',
	        'min'       => 'El campo :attribute no puede tener menos de :min carácteres.',
	        'max'       => 'El campo :attribute no puede tener más de :min carácteres.'
	    );
    
    	$validation = Validator::make($inputs, $rules, $messages);
 
    	if($validation->fails()){
    		return $validation;
    	}else{
    		return true;
    	}
 
	}
		
	private function revisarComentario($comentario, $id_post, $id_user){
		if ($this->validateForms($comentario, $id_post, $id_user) === true) {
			return "inicio";
		} else {
			return 'detalle-post/'.$id_post;
		}
		
	}
}

?>