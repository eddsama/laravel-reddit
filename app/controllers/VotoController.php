<?php
/**
* 
*/
class VotoController extends BaseController
{
	
	public function votar(){
		
		$siVoto = Voto::where('FK_user','=',Auth::User()->id)
					->where('FK_post','=',Input::get('post'))
					->count();

		if (!$siVoto) {// si no ha votado

			$post = Post::find(Input::get('post'));
			$post->like += 1;//Input::get('numVotos')

			$voto = new Voto();
			$voto->FK_user = Auth::User()->id;
			$voto->FK_post = $post->id;
			if ($post->save() && $voto->save()) {
				
				return Redirect::to('inicio')->with('mensaje', 'Has votado!');
			} else {
				return Redirect::to('inicio')->with('mensaje', 'no se pudo realizar el voto');
			}
		} else {
			return Redirect::to('inicio')->with('mensaje', 'Ya has votado Previamente esta Publicación');
		}		
	}

	public function difamar(){

		$siVoto = Voto::where('FK_user','=',Auth::User()->id)
					->where('FK_post','=',Input::get('post'))
					->count();

		if ($siVoto) {// Si ya ha votado conanterioridad

			$post = Post::find(Input::get('post'));
			$post->like -= 1;//Input::get('numVotos')

			//$siVoto = Voto::find();
			//Esta borrando todo lo que se le dala gana por cada usuairo
			Voto::where('FK_user','=',Auth::User()->id)
					->where('FK_post','=',Input::get('post'))->delete();
			/*
			$voto = Voto::find($id);
			$voto->delete();
			//$post->delete();
			/*$id = Voto::where('FK_user','=',Auth::User()->id)
						 	  ->where('FK_post','=',Input::get('post'))
							  ->get('id');
			Voto::destroy($id);*/

			if ($post->save()) {
				return Redirect::to('inicio')->with('mensaje', 'Has Difamado al Vale...');
			} else {
				return Redirect::to('inicio')->with('mensaje', 'no se pudo realizar el voto');
			}
		} else {
			return Redirect::to('inicio')->with('mensaje', 'Solo Puedes difamar una vez');
		}		
		
	}

}