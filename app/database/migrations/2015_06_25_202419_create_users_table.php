<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($tabla){
			$tabla->increments('id');
			$tabla->string('usuario',100)->unique();
			$tabla->string('email',100)->unique();
			$tabla->string('password',100);
			$tabla->timestamps();
		});
    }
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
