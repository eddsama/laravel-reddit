<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLikeYDislike extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('votos', function($tabla){
			$tabla->increments('id');
			$tabla->integer('FK_user');
			$tabla->integer('FK_post');
		});
		/*Schema::table('posts', function($tabla){
			$tabla->integer('like');
			$tabla->integer('dislike');
		});*/
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('votos');
	}

}
