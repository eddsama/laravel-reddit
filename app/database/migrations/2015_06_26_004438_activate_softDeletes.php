<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ActivateSoftDeletes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('posts', function($tabla){
			$tabla->softDeletes();
		});
		Schema::table('users', function($tabla){
			$tabla->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('posts', function($tabla){
			$tabla->dropColumn('deleted_at');
		});
		Schema::table('users', function($tabla){
			$tabla->dropColumn('deleted_at');
		});
	}

}
