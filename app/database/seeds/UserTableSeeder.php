<?php

/**
* 
*/
class UserTableSeeder extends Seeder
{
	
	public function run(){
		User::create(array(
			'usuario' => 'admin',
			'email' => 'eduardosam.bash@gmail.com',
			'password' => Hash::make('admin')
			// Hash::make() contraseña encriptada
			));
	}
}